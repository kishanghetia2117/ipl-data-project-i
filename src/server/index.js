console.clear();
const csv = require("csvtojson");
const fs = require("fs");

const csvFileOne = "./src/data/matches.csv";
const csvFileTwo = "./src/data/deliveries.csv";

const ipl = require("./ipl.js");

const runIplFunction = (matchesCsvFilePath, deliveriesCsvFilePath, ipl) => {
    
    csv().fromFile(matchesCsvFilePath).then((matches) => {
        csv().fromFile(deliveriesCsvFilePath).then((deliveries) => {
            
            function getOutput(matches, deliveries, iplFunction, outputFilePath) {
                let result = iplFunction(matches, deliveries);
                fs.writeFile(outputFilePath, JSON.stringify(result), (err) => {
                    if (err) console.log(err);
                })
            }
            
            let outputFilePath = "./src/public/output/matchesPerYear.json";
            getOutput(matches, deliveries, ipl.matchesPlayedPerYear, outputFilePath);

            outputFilePath = "./src/public/output/teamWinCount.json";
            getOutput(matches, deliveries, ipl.teamWinCountPerYear, outputFilePath);

            outputFilePath = "./src/public/output/extraRunsConcededPer.json";
            getOutput(matches, deliveries, ipl.extraRunsConcededPerTeam, outputFilePath);

            outputFilePath = "./src/public/output/economicalBowlers.json";
            getOutput(matches, deliveries, ipl.economicalBowlersOfYear, outputFilePath);
            
            outputFilePath = "./src/public/output/batsmanStrikeRatePerSeason.json";
            getOutput(matches, deliveries, ipl.batsmanStrikeRatePerSeason, outputFilePath);

            outputFilePath = "./src/public/output/wonTossAndMatch.json";
            getOutput(matches, deliveries, ipl.wonTossAndMatch, outputFilePath);

            outputFilePath = "./src/public/output/manOfTheMatch.json";
            getOutput(matches, deliveries, ipl.manOfTheMatch, outputFilePath);

            outputFilePath = "./src/public/output/highestTimePlayerdismissed.json";
            getOutput(matches, deliveries, ipl.highestTimePlayerdismissed, outputFilePath);

            outputFilePath = "./src/public/output/bestEconomySperOvers.json";
            getOutput(matches, deliveries, ipl.bestEconomySperOvers, outputFilePath);  
        })
    })
}

// runIplFunction contains 2 input filepath 1 function module set
runIplFunction(csvFileOne, csvFileTwo, ipl);