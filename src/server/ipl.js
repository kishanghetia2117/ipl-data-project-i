// Number of matches played per year for all the years in IPL.
const matchesPlayedPerYear = (matchdata) => {

    let getMatchesPlayedPerYear = (matchesPerYear, currentMatch) => {
        if (matchesPerYear.hasOwnProperty(currentMatch.season)) {
            matchesPerYear[currentMatch.season] += 1;
        } else {
            matchesPerYear[currentMatch.season] = 1;
        }

        return matchesPerYear;
    }
    return matchdata.reduce(getMatchesPlayedPerYear, {});
}

// Number of matches won per team per year in IPL.
const teamWinCountPerYear = (matchdata) => {

    let getTeamWinCountPerYear = (winCountPerYear, currentMatch) => {
        if (winCountPerYear.hasOwnProperty(currentMatch.season)) {
            if (winCountPerYear[currentMatch.season].hasOwnProperty(currentMatch.winner)) {
                winCountPerYear[currentMatch.season][currentMatch.winner] += 1;
            }
            else if (currentMatch.winner != '') {
                winCountPerYear[currentMatch.season][currentMatch.winner] = 1;
            }
        } else {
            winCountPerYear[currentMatch.season] = {};
            winCountPerYear[currentMatch.season][currentMatch.winner] = 1;
        }
        return winCountPerYear;
    }

    return matchdata.reduce(getTeamWinCountPerYear, {});
}

// Extra runs conceded per team in the year 2016
const extraRunsConcededPerTeam = (matchdata, deliveriedata) => {
    const objOfYear = matchdata.filter(match => match.season == 2016);
    const idOfobjOfYear = objOfYear.map(match => match.id);

    let getextraRunsConcededPerTeam = (runsConceded, currentMatch) => {
        if (idOfobjOfYear.includes(currentMatch.match_id)) {
            if (runsConceded.hasOwnProperty(currentMatch.bowling_team)) {
                runsConceded[currentMatch.bowling_team] += parseInt(currentMatch.extra_runs);
            } else {
                runsConceded[currentMatch.bowling_team] = parseInt(currentMatch.extra_runs);
            }
        }
        return runsConceded;
    }

    return deliveriedata.reduce(getextraRunsConcededPerTeam, {});
}

// Top 10 economical bowlers in the year 2015
const economicalBowlersOfYear = (matchdata, deliveriedata) => {
    const objOfYear = matchdata.filter(match => match.season == 2015);
    const idOfobjOfYear = objOfYear.map(match => match.id);

    let getEconomicalBowlersOfYear = (bowlerEconomi, currentMatch) => {
        if (idOfobjOfYear.includes(currentMatch.match_id)) {
            if (bowlerEconomi.hasOwnProperty(currentMatch.bowler)) {
                let balls = bowlerEconomi[currentMatch.bowler].ball += 1;
                let totalRuns = bowlerEconomi[currentMatch.bowler].total_runs += parseInt(currentMatch.total_runs);
                bowlerEconomi[currentMatch.bowler].economi = totalRuns / (balls / 6);
            } else {
                bowlerEconomi[currentMatch.bowler] = {};
                let balls = bowlerEconomi[currentMatch.bowler].ball = 1;
                let totalRuns = bowlerEconomi[currentMatch.bowler].total_runs = parseInt(currentMatch.total_runs);
                bowlerEconomi[currentMatch.bowler].economi = totalRuns / (balls / 6);
            }
        }
        return bowlerEconomi;
    }
    const economiData = deliveriedata.reduce(getEconomicalBowlersOfYear, {});

    // arr that contains only economi from reduced deliveriedata
    let economiDataArray = []
    for (let property in economiData) {
        economiDataArray.push(economiData[property].economi);
    }

    //sort it in acending order
    economiDataArray.sort(function (a, b) {
        return a - b;
    });

    // gets top 10 economi bowlers info 
    let topTeneconomiBowlersOfYear = {};
    for (let index = 0; index < 10; index++) {
        for (let propertie in economiData) {
            if (economiData[propertie].economi == economiDataArray[index]) {
                topTeneconomiBowlersOfYear[propertie] = economiData[propertie].economi;
            }
        }
    }

    return topTeneconomiBowlersOfYear;
}

// doubt how can be checking be efficient match years to delveries or cluch delivers and then check at the end ?
// Find the strike rate of a batsman for each season
const batsmanStrikeRatePerSeason = (matchdata, deliveriedata) => {

    //create seprate arr that conatins match id based on years
    let getIdForEachSeason = (idPerSeason, currentMatch) => {
        if (idPerSeason.hasOwnProperty(currentMatch.season)) {
            if (!idPerSeason[currentMatch.season].includes(currentMatch.id)) {
                idPerSeason[currentMatch.season].push(currentMatch.id);
            }
        } else {
            idPerSeason[currentMatch.season] = [];
            idPerSeason[currentMatch.season].push(currentMatch.id);
        }
        return idPerSeason;
    }
    const idForEachSeason = matchdata.reduce(getIdForEachSeason, {});

    // gets strikerate of a player for each season from dataset using reduce 
    let getStrikeRate = (strikeRate, currentMatch) => {
        if (currentMatch.batsman == "MS Dhoni") {
            if (strikeRate.hasOwnProperty(currentMatch.batsman)) {
                //uses filter to find the season for the match id from seprate dataset
                let MSdhonistrikeRate = Object.entries(idForEachSeason).filter(ele => {
                    return ele[1].includes(currentMatch.match_id);
                })
                if (strikeRate[currentMatch.batsman].hasOwnProperty([MSdhonistrikeRate[0][0]])) {
                    let ballsPlayed = strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]].ball += 1;
                    let runsScored = strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]].total_runs += parseInt(currentMatch.total_runs);
                    strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]].strike_rate = (runsScored / ballsPlayed) * 100;
                } else {
                    strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]] = {};
                    let ballsPlayed = strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]].ball = 1;
                    let runsScored = strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]].total_runs = parseInt(currentMatch.total_runs);
                    strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]].strike_rate = (runsScored / ballsPlayed) * 100;
                }
            }
            else {
                strikeRate[currentMatch.batsman] = {};
                //uses filter to find the season for the match id from seprate dataset
                let MSdhonistrikeRate = Object.entries(idForEachSeason).filter(ele => {
                    return ele[1].includes(currentMatch.match_id);
                })
                strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]] = {};
                let ballsPlayed = strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]].ball = 1;
                let runsScored = strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]].total_runs = parseInt(currentMatch.total_runs);
                strikeRate[currentMatch.batsman][MSdhonistrikeRate[0][0]].strike_rate = (runsScored / ballsPlayed) * 100;
            }
        }
        return strikeRate;
    }
    return deliveriedata.reduce(getStrikeRate, {});
}


// Find the number of times each team won the toss and also won the match
const wonTossAndMatch = (matchdata) => {

    let getWonTossAndMatch = (countOfTossAndMatch, currentMatch) => {
        if (countOfTossAndMatch.hasOwnProperty(currentMatch.winner)) {
            if (currentMatch.winner == currentMatch.toss_winner) {
                countOfTossAndMatch[currentMatch.winner] += 1;
            }
        } else if (currentMatch.winner !== '') {
            if (currentMatch.winner == currentMatch.toss_winner) {
                countOfTossAndMatch[currentMatch.winner] = 1;
            } else {
                countOfTossAndMatch[currentMatch.winner] = 0;
            }
        }
        return countOfTossAndMatch;
    }

    return matchdata.reduce(getWonTossAndMatch, {});
}

// Find a player who has won the highest number of Player of the Match awards for each season
const manOfTheMatch = (matchdata) => {
    //get all man of the match count per season 
    let getManOfTheMatch = (manOfTheMatchCount, currentMatch) => {
        if (manOfTheMatchCount.hasOwnProperty(currentMatch.season)) {
            if (manOfTheMatchCount[currentMatch.season].hasOwnProperty(currentMatch.player_of_match)) {
                manOfTheMatchCount[currentMatch.season][currentMatch.player_of_match] += 1;
            } else {
                manOfTheMatchCount[currentMatch.season][currentMatch.player_of_match] = 1;
            }
        } else {
            manOfTheMatchCount[currentMatch.season] = {};
            manOfTheMatchCount[currentMatch.season][currentMatch.player_of_match] = 1;
        }
        return manOfTheMatchCount;
    }
    const onjGetManOfTheMatch = matchdata.reduce(getManOfTheMatch, {});

    //sort it and return it for whole dataset
    let arrOfManOfTheMatch = Object.keys(onjGetManOfTheMatch).map((year) => {
        let playerArr = Object.entries(onjGetManOfTheMatch[year]).sort((current, previous) => previous[1] - current[1]);
        return [year, playerArr];
    });

    // doubt is the method right called map but used object ousdiete the scope and then returened the object that is outside
    topSeasonManOfTheMatch = {};
    let topWinnerCount = 0;

    // find all top man of the match from the sorted arr for all season
    let topManOfMatch = (year) => {
        year[1].map((awardCount) => {
            if (topSeasonManOfTheMatch.hasOwnProperty(year[0])) {
                if (!topSeasonManOfTheMatch[year[0]].hasOwnProperty(awardCount[0]) && awardCount[1] == topWinnerCount) {
                    topSeasonManOfTheMatch[year[0]][awardCount[0]] = awardCount[1];
                }
            } else {
                topSeasonManOfTheMatch[year[0]] = {};
                topSeasonManOfTheMatch[year[0]][awardCount[0]] = awardCount[1];
                topWinnerCount = awardCount[1];
            }
            return topSeasonManOfTheMatch;
        })
        return topSeasonManOfTheMatch;
    }
    arrOfManOfTheMatch.map(topManOfMatch);

    // return final output
    return topSeasonManOfTheMatch;
}

// Find the highest number of times one player has been dismissed by another player
const highestTimePlayerdismissed = (matchdata, deliveriedata) => {
    // gets all the credit count with name for a particular batsman
    let gethighestTimePlayerdismissed = (playerdismissCount, currentMatch) => {
        //assigned who gets credit for dismiss
        bowlerCredit = [
            'caught',
            'bowled',
            'lbw',
            'caught and bowled',
            'stumped',
            'hit wicket'
        ];
        filderCredit = [
            'caught',
            'run out',
            'stumped'
        ];

        if (playerdismissCount.hasOwnProperty(currentMatch.player_dismissed)) {
            if (bowlerCredit.includes(currentMatch.dismissal_kind)) {
                if (playerdismissCount[currentMatch.player_dismissed].hasOwnProperty(currentMatch.bowler)) {
                    playerdismissCount[currentMatch.player_dismissed][currentMatch.bowler] += 1;
                } else {
                    playerdismissCount[currentMatch.player_dismissed][currentMatch.bowler] = 1;
                }
            }
            if (filderCredit.includes(currentMatch.dismissal_kind)) {
                if (playerdismissCount[currentMatch.player_dismissed].hasOwnProperty(currentMatch.fielder)) {
                    playerdismissCount[currentMatch.player_dismissed][currentMatch.fielder] += 1;
                } else {
                    playerdismissCount[currentMatch.player_dismissed][currentMatch.fielder] = 1;
                }
            }
        } else if (!currentMatch.player_dismissed == '') {
            playerdismissCount[currentMatch.player_dismissed] = {};
            if (bowlerCredit.includes(currentMatch.dismissal_kind)) {
                playerdismissCount[currentMatch.player_dismissed][currentMatch.bowler] = 1;
            }
            if (filderCredit.includes(currentMatch.dismissal_kind)) {
                playerdismissCount[currentMatch.player_dismissed][currentMatch.fielder] = 1;
            }
        }
        return playerdismissCount;
    }
    playerdismissCount = deliveriedata.reduce(gethighestTimePlayerdismissed, {});

    // sort creadit score in descending for each batsman
    let sortedPlayerdismissCount = Object.keys(playerdismissCount).map((batsman) => {
        let sortedPlayerDismiseed = Object.entries(playerdismissCount[batsman]).sort(function (current, previous) {
            return previous[1] - current[1];
        })
        return [batsman, sortedPlayerDismiseed];
    })

    // sort creadit score in descending between different batsman after sorting the bowler
    let getBatsmanDismmiedHIghestBy = (batsman1dismissalReport, batsman2dismissalReport) => {
        return batsman2dismissalReport[1][0][1] - batsman1dismissalReport[1][0][1];
    }
    let sortedplayerDismissed = sortedPlayerdismissCount.sort(getBatsmanDismmiedHIghestBy);

    // asign the top value from 4-d array to get the output
    let mostDismissedPlayerByPlayer = {};
    mostDismissedPlayerByPlayer.batsman = sortedplayerDismissed[0][0];
    mostDismissedPlayerByPlayer.bowler = sortedplayerDismissed[0][1][0][0];
    mostDismissedPlayerByPlayer.dismissCount = sortedplayerDismissed[0][1][0][1];
    return mostDismissedPlayerByPlayer;
}

// Find the bowler with the best economy in super overs
const bestEconomySperOvers = (matchdata, deliveriedata) => {
// gets superover data for all the seasons
    let getbestEconomySuperOvers = (getSuperOvers, currentMatch) => {
        if (currentMatch.is_super_over === '1') {
            if (getSuperOvers.hasOwnProperty(currentMatch.bowler)) {
                getSuperOvers[currentMatch.bowler].ball += 1;
                getSuperOvers[currentMatch.bowler].total_runs += parseInt(currentMatch.total_runs);

                let over = getSuperOvers[currentMatch.bowler].ball / 6;
                let economi = getSuperOvers[currentMatch.bowler].total_runs / over;
                getSuperOvers[currentMatch.bowler].economi = economi;

            } else {
                getSuperOvers[currentMatch.bowler] = {};
                getSuperOvers[currentMatch.bowler].ball = 1;
                getSuperOvers[currentMatch.bowler].total_runs = parseInt(currentMatch.total_runs);

                let over = getSuperOvers[currentMatch.bowler].ball / 6;
                let economi = getSuperOvers[currentMatch.bowler].total_runs / over;
                getSuperOvers[currentMatch.bowler].economi = economi;
            }
        }
        return getSuperOvers;
    }
    const superOversInfo = deliveriedata.reduce(getbestEconomySuperOvers, {});

    // sort the superoverdata
    const sortedSuperOversInfo = Object.entries(superOversInfo).sort((a, b) => {
        return a[1].economi - b[1].economi;
    })

    //gest top value from superoverdata
    let topEconomySuperOvers = {};
    topEconomySuperOvers[sortedSuperOversInfo[0][0]] = sortedSuperOversInfo[0][1].economi;
    return topEconomySuperOvers;
}


module.exports = {
    matchesPlayedPerYear,
    teamWinCountPerYear,
    extraRunsConcededPerTeam,
    economicalBowlersOfYear,
    batsmanStrikeRatePerSeason,
    wonTossAndMatch,
    manOfTheMatch,
    highestTimePlayerdismissed,
    bestEconomySperOvers
};